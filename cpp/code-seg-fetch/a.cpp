#include <algorithm>
#include <iostream>

#define MX 100000
#define RND 2000

using namespace std;

long counter(int array[])
{
    long c = 0;
    for (int i = 0; i < RND; i++)
        for (int j = 0; j < MX; j++)
            if (array[j] < 5000)
                c++;
    return c;
}

int main()
{
    srand(time(NULL));

    int arr[MX];
    clock_t start, end;

    {
        start = clock();
        cout << "filled:\t\t";
        for (int i = 0; i < MX; i++)
            arr[i] = rand() % 10000;
        end = clock();
        cout << MX << " * " << RND << "\t";
        cout << (end - start) * 1000. / CLOCKS_PER_SEC << "ms" << endl;
    }

    {
        start = clock();
        cout << "not sorted:\t" << counter(arr) << "\t";
        end = clock();
        cout << (end - start) * 1000. / CLOCKS_PER_SEC << "ms" << endl;
    }

    {
        start = clock();
        sort(std::begin(arr), std::end(arr));
        end = clock();
        cout << "sorted in:\t\t\t" << (end - start) * 1000. / CLOCKS_PER_SEC << "ms" << endl;
    }

    {
        start = clock();
        cout << "sorted:\t\t" << counter(arr) << "\t";
        end = clock();
        cout << (end - start) * 1000. / CLOCKS_PER_SEC << "ms" << endl;
    }
}

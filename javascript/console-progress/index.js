const ProgressBar = require('progress')

const bar = new ProgressBar(':current/:total [:bar] :percent', { total: 20 })
const timer = setInterval(() => {
  bar.tick()
  if (bar.complete) {
    console.log('\ncomplete\n')
    clearInterval(timer)
  }
}, 100)

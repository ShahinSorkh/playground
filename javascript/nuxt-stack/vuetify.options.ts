import '@fortawesome/fontawesome-free/css/all.css'
// import 'roboto-fontface/css/roboto/roboto-fontface.css'

import colors from 'vuetify/es5/util/colors'
// import en from 'vuetify/src/locale/en'
// import fa from 'vuetify/src/locale/fa'

export default {
  rtl: true,
  theme: {
    dark: true,
    options: {
      customProperties: true
    },
    themes: {
      dark: {
        primary: colors.blue.darken2,
        accent: colors.grey.darken3,
        secondary: colors.amber.darken3,
        info: colors.teal.lighten1,
        warning: colors.amber.base,
        error: colors.deepOrange.accent4,
        success: colors.green.accent3
      }
    }
  },
  // lang: {
  //   locales: { en, fa },
  //   current: 'fa'
  // },
  icons: {
    iconfont: 'fa'
  }
}

import Axios, { AxiosInstance, AxiosRequestConfig } from 'axios'
import Vue, { PluginFunction } from 'vue'

interface AxiosPlugin {
  install: PluginFunction<any>
  version: string
}

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

let config: AxiosRequestConfig = {
  // baseURL: process.env.baseURL || process.env.apiUrl || '',
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true // Check cross-site Access-Control
}

const _axios: AxiosInstance = Axios.create(config)

// _axios.interceptors.request.use(
//   config => {
//     // Do something before request is sent
//     return config
//   },
//   error => {
//     // Do something with request error
//     return Promise.reject(error)
//   }
// )

// _axios.interceptors.response.use(
//   response => {
//     // Do something with response data
//     return response
//   },
//   error => {
//     // Do something with response error
//     return Promise.reject(error)
//   }
// )

const axiosPlugin: AxiosPlugin = {
  install: (Vue, options?: object) => {
    Vue.$http = _axios
    Vue.prototype.$http = {
      get: () => _axios
    }
  },
  version: '0.19'
}

Vue.use(axiosPlugin)

export default axiosPlugin

import '@fortawesome/fontawesome-free/css/all.css'
import 'roboto-fontface/css/roboto/roboto-fontface.css'

import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import en from 'vuetify/src/locale/en'
import fa from 'vuetify/src/locale/fa'

Vue.use(Vuetify)

export default new Vuetify({
  rtl: true,
  theme: {
    dark: true,
    options: {
      customProperties: true
    },
    themes: {
      light: {
        primary: '#ee44aa',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      }
    }
  },
  lang: {
    locales: { en, fa },
    current: 'fa'
  },
  icons: {
    iconfont: 'fa'
  }
})

import { AxiosInstance } from 'axios'

declare module 'vue/types/vue' {
  export interface VueConstructor {
    $http: AxiosInstance
  }

  export interface Vue {
    $http: AxiosInstance
  }
}

# pylint: disable=E0213,E1101,E1102,R0201,W0122,W0703
""" Evaluates python code safely """

import datetime
import re
import threading
import time
import pytz
import jdatetime
import _thread


class Evaluator:
    ''' Evaluator object '''

    def __init__(self, local_env=None, timeout=2, log=False):
        if local_env is None:
            local_env = dict()

        self.builtins = {
            'int': int, 'str': str, 'list': list, 'tuple': tuple,
            'float': float, 'dict': dict, 'abs': abs, 'bool': bool,
            'chr': chr, 'enumerate': enumerate, 'len': len, 'max': max,
            'min': min, 'range': range, 'sum': sum, 'set': set,
            'divmod': divmod, 'all': all, 'any': any, 'map': map,
            'zip': zip, 'print': self.__print, 'dir': dir
        }
        self.local_env = {
            're': re,
            'datetime': datetime,
            'jdatetime': jdatetime,
            'time': time,
            'pytz': pytz
        }
        self.local_env.update(local_env)
        self.timeout = timeout
        self.log = log

    def __print(self, *args):
        if self.log:
            print(' '.join(map(str, args)))

    def __exit_after(func):
        '''
        use as decorator to exit process if
        function takes longer than s seconds
        '''

        def inner(self, *args, **kwargs):
            ''' inner wrapper '''

            timer = threading.Timer(
                self.timeout,
                lambda x: _thread.interrupt_main(),
                args=[func.__name__]
            )
            timer.start()
            try:
                result = func(self, *args, **kwargs)
            finally:
                timer.cancel()
            return result
        return inner

    def prepare_eval(self, eval_str):
        ''' remove dangerous code from the eval_str '''

        eval_str = re.sub(
            r'(?:import|from)\b\s+\b(?:(?:\b[\w\t\v\r ,]+\b)' +
            r'|\(\b[\w\t\v\r ,]+\b\))\b',
            '', eval_str)
        eval_str = re.sub(
            r"(?:\b__\w+__\b|`.*`|(?:#[^\n]*|'{3}[\s\S]*'{3}))",
            '',
            eval_str)
        eval_str = re.sub(r'^[^\S\n]+$', '\n', eval_str, re.M)
        eval_str = re.sub(r'\n{3,}', '\n\n', eval_str)
        return eval_str.strip()

    @__exit_after
    def _run_eval(self, eval_str, arg):
        self.__print('-' * 80)
        self.__print(eval_str)
        self.__print('-' * 80)

        global_env = {
            '__name__': None,
            '__file__': None,
            '__builtins__': self.builtins
        }
        self.local_env['arg'] = arg

        exec(eval_str, global_env, self.local_env)
        return self.local_env.get('arg', None)

    def eval(self, eval_str, arg):
        ''' Evaluate the input string in isolated environment
            and mutate arg argument '''
        eval_str = self.prepare_eval(eval_str)

        try:
            return self._run_eval(eval_str, arg)
        except KeyboardInterrupt:
            self.__print(
                '==' * 10,
                'EVAL EXCEPTION',
                '==' * 10,
                ':',
                'execution took too long')
            return None
        except Exception as ex:
            self.__print(repr(ex))
            self.__print('=' * 10, 'EVAL EXCEPTION', '=' * 10, ':', ex)
            return None


if __name__ == "__main__":
    import sys
    EVALUATOR = Evaluator(local_env={'sys': sys}, log=True)
    A = '1397-09-25T15:09:00+0440'
    print(A, '\n')
    with open('command', 'r') as cmd_file:
        print('eval\n', EVALUATOR.eval(cmd_file.read(), A))

# pylint: disable=W0703,W0123,C0103
''' test evaluator '''

import re


def evaluate(arg, eval_str):
    ''' test func '''

    global_env = {
        're': re,
        '__name__': None,
        '__file__': None,
        '__builtins__': {
            'int': int,
            'str': str,
            'list': list,
            'tuple': tuple,
            'float': float,
            'dict': dict,
            'abs': abs,
            'bool': bool,
            'chr': chr,
            'enumerate': enumerate,
            'len': len,
            'max': max,
            'min': min,
            'range': range,
            'round': round,
            'sum': sum,
            'set': set,
            'map': map,
            'long': int,
            'divmod': divmod,
            'all': all,
            'any': any,
            'zip': zip
        }
    }
    local_env = {'arg': arg}
    try:
        return str(eval(eval_str, global_env, local_env))
    except Exception as e:
        return ' '.join(['=' * 5, 'EVAL EXCEPTION', '=' * 5, str(e)])


A = '42b2bu3d0238b-7402fbsk3407'
print('a: ', A)
print('e: ', evaluate(A, r'[1 while 1]'))
